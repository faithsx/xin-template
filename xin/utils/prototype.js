import Vue from 'vue'

// 是否登录
export const isLogin = () => {
	var _userInfo = uni.getStorageSync('userInfo');
	if (_userInfo?.detail) return true
	if (!_userInfo) {
		Vue.prototype.$showModal({
			title: '提示',
			content: '还未登录，请先登录',
			showCancel: true,
			cancelText: '否',
			confirmText: '去登录',
			success: res => {
				if (res.confirm) {
					uni.navigateTo({
						url: '/user/login/login',
						fail: res => {
							console.log(JSON.stringify(res))
						}
					})
				}
			}
		});
	} else if (!_userInfo.detail) {
		Vue.prototype.$showModal({
			content: '账号异常'
		});
		resetStorage()
	}
	return false
}


// 是否登录  不提示弹窗
export const isLoginBool = () => {
	var _userInfo = uni.getStorageSync('userInfo');
	if (_userInfo?.detail) return true
	return false
}


// 播放指定文本音频
export const playAudio = (text) => {
	var innerAudioContext = uni.createInnerAudioContext();
	innerAudioContext.autoplay = true;
	innerAudioContext.src = 'http://tts.baidu.com/text2audio?cuid=baiduid&lan=zh&ctp=1&pdt=12&tex=' + text;
}

// 跳转小程序
export const openMini = (path) => {
	plus.share.getServices(res => {
		let sweixin = null;
		for (let i in res) {
			if (res[i].id == 'weixin') {
				sweixin = res[i];
			}
		}
		if (sweixin) {
			sweixin.launchMiniProgram({
				id: 'gh_0f738a380461', //
				type: 0, //小程序版本  0-正式版； 1-测试版； 2-体验版。
				path //小程序的页面,用传的参数在小程序接值判断跳转指定页面
			});
		}
	});
}

// 是否绑定小区
export const isBindXiaoQu = () => {
	if (uni.getStorageSync('village')?.id) return true;
	Vue.prototype.$showModal({
		title: '提示',
		content: '还未绑定当前小区，请前往"我的 -> 更换小区"',
		showCancel: true,
		cancelText: '不去',
		confirmText: '去切换',
		success: res => {
			if (res.confirm) {
				uni.switchTab({
					url: '/pages/user/user'
				})
			}
		}
	});
	return false
}

// 重置所有缓存
export const resetStorage = () => {
	uni.clearStorageSync();
}
