// 常用的操作
import * as prototypes from './utils/prototype.js'
// 常用的字符串操作
import * as filters from './utils/filters.js'
// 统一app和小程序弹窗
import show_modal from './components/show_modal/show_modal.js'
// 微信小程序线上输出日志
import wxlog from './utils/wxlog.js'
// api统一管理
import api from './api/index.js';
// colorui封装的顶部导航栏
import cuCustom from './components/colorui/components/cu-custom.vue'
// 工具集合
import * as tool from './utils/tool.js'
// 配置文件
import * as config from './config/index.js'
// cnpm -i --save @pyramid-kit/mobile-eco
// 获取当前平台（web 网页 / 安卓 app / IOS app / 微信网页（公众号） / 微信小程序 / 支付宝小程序 / 客户端开发工具 / 云闪付小程序）
/*import {
	platform, // 字符串结果
	EPlatform // 枚举
} from '@pyramid-kit/mobile-eco';*/


const install = Vue => {
	Vue.prototype.$api = api
	Vue.prototype.$wxlog = wxlog
	Vue.prototype.$config = config
	Vue.prototype.$tool = Object.assign(filters, tool)
	Vue.use(show_modal)
	Vue.component('cu-custom', cuCustom)
	Object.keys(prototypes).forEach(key => {
		Vue.prototype[key] = prototypes[key]
	})
	Object.keys(filters).forEach(key => {
		Vue.filter(key, filters[key])
	})
}

export default install
