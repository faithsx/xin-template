import wxlog from '../utils/wxlog.js'
import {
	config
} from '../config/index.js'

exports.request = (prefix, obj = {}, api, method) => {
	if (!beforeRequest()) return
	let accessToken = uni.getStorageSync('userInfo').accessToken
	let url = '/' + prefix + '/' + api;
	let data = obj.data || {};
	let success = obj.success;
	let fail = obj.fail;
	let complete = obj.complete;
	let header = obj.header || {}
	header.accessToken = accessToken
	header['Content-Type'] = header.json ?
		'application/json' :
		'application/x-www-form-urlencoded; charset=UTF-8';
	if (obj.header)
		if (obj.header.json) delete obj.header.json
	uni.request({
		url,
		data,
		method: method.toUpperCase(),
		header,
		// 请求成功会执行
		success: (res) => {
			if (res.data.code != 200) errlog('success', res.data, data, api)
			if (success) success(res)
		},
		// 请求失败会执行
		fail: err => {
			console.log(err);
			errlog('fail', err, data, api)
			if (fail) fail(err)
		},
		//无论如何都会执行
		complete: (data) => {
			afterRequest()
			if (complete) complete(data)
		}
	})
}

// Promise
exports.synchro = (prefix, obj = {}, api, method) => {
	return new Promise((resolve, reject) => {
		this.request(prefix, {
			url: obj.url,
			data: obj.data,
			method: obj.method,
			header: obj.header,
			success: (res) => {
				resolve(res)
			},
			fail: err => {
				reject(err)
			}
		}, api, method);
	});
}

// 请求前
function beforeRequest() {
	if (!navigator.onLine) {
		uni.showToast({
			title: '网络异常，请检查网络！',
			icon: 'none'
		})
		return false;
	}
	if (config.requestLoading) {
		uni.showLoading({
			mask: true,
			title: config.requestLoadingTitle
		})
	}
	return true
}

// 请求后
function afterRequest() {
	if (config.requestLoading) {
		uni.hideLoading()
	}
}

// Xml转换Json对象
function xmlToJson(xmlStr) {
	var parser = new DOMParser();
	var xmlDoc = parser.parseFromString(xmlStr, "text/xml");
	var nodes = xmlDoc.firstElementChild.textContent;
	return JSON.parse(nodes)
}

// 错误日志
function errlog(type, info, data, api) {
	// #ifdef MP-WEIXIN
	// 如果不是开发环境，将在小程序后台打印错误日志
	if (uni.getAccountInfoSync().miniProgram.envVersion != 'develop') {
		var tr = false
		for (var ex of excludes_fail) {
			if (url.indexOf(ex) > -1) tr = true
		}
		if (!tr) wxlog.error(type + '：接口 ' + api + ' 发生错误。\n错误信息：', JSON.stringify(info), '\n参数：', data)
	}
	// #endif
}
