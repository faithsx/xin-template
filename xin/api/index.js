import {
	request,
	synchro
} from './request.js';

export default {
	// 登录
	miniLogin: obj => request('lei', obj, 'SYTJ', 'GET')
}

/*
需将接口请求方式改为synchro
this.$api.login({
		data: {
			jsCode: '123456789'
		}
	})
	.then(
		ret => {
			console.log(ret);
		},
		rej => {
			console.log(rej);
		}
	);			
*/

/*
let ret = await this.$api.login({
	data: {
		jsCode: '123456789'
	}
});	
*/


/*
login(code) {
	this.$api.login({
		data: {
			jsCode: '123456789'
		},
		success: res => {
			console.log(res);
		},
		complete: data => {
			无论如何都会执行
		}
	});
}
*/
