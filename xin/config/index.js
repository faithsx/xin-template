// 请求域名前缀
exports.baseUrl = {
	lei: 'http://lilei.vaiwan.com/WebServiceZXWYDP.asmx/',
	peng: 'http://192.168.1.2:8011/WebService2.asmx/'
}

exports.config = {
	// 每次请求前是否需要显示加载中
	requestLoading: true,
	// 加载提示
	requestLoadingTitle: '加载中...'
}
