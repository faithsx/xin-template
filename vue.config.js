const {
	baseUrl
} = require('./xin/config/index.js');

module.exports = {
	devServer: {
		disableHostCheck: true,
		proxy: (() => {
			var proxys = {}
			Object.entries(baseUrl).forEach(item => {
				proxys['/' + item[0]] = {
					target: item[1],
					changOrigin: true,
					pathRewrite: {
						['/' + item[0]]: ''
					}
				}
			})
			return proxys
		})()
	},
	chainWebpack: config => {
		// 使用webpack.DefinePlugin
		config.plugin('define').tap(args => {
			args[0]['process.env'].VERSION = (function() {
				const now = new Date()
				return now.getFullYear() + format(now.getMonth() + 1) + format(now.getDate()) +
					format(now.getHours()) + format(now.getMinutes())

				function format(num) {
					return num < 10 ? '0' + num : '' + num
				}
			})()
			return args
		})
	}
}
